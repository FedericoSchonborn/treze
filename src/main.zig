const std = @import("std");
const consts = @import("./consts.zig");

const THIRTEEN = consts.THIRTEEN;

pub fn is(comptime T: type, value: T) type {
    return struct {
        pub fn thirteen() bool {
            return value == THIRTEEN;
        }

        pub const returning = struct {
            pub fn thirteen() bool {
                return value() == THIRTEEN;
            }
        };

        pub const not = struct {
            pub fn thirteen() bool {
                return value != THIRTEEN;
            }
        };

        pub const divisible = struct {
            pub const by = struct {
                pub fn thirteen() bool {
                    return value % THIRTEEN == 0;
                }
            };
        };

        pub const square = struct {
            pub const of = struct {
                pub fn thirteen() bool {
                    return value == THIRTEEN * THIRTEEN;
                }
            };
        };

        pub const greater = struct {
            pub const than = struct {
                pub fn thirteen() bool {
                    return value > THIRTEEN;
                }
            };
        };

        pub const less = struct {
            pub const than = struct {
                pub fn thirteen() bool {
                    return value < THIRTEEN;
                }
            };
        };

        pub fn within(range: T) type {
            return struct {
                pub const of = struct {
                    pub fn thirteen() bool {
                        return value > (THIRTEEN - range) and value < (THIRTEEN + range);
                    }
                };
            };
        }

        pub fn plus(amount: T) type {
            return is(T, value + amount);
        }

        pub fn minus(amount: T) type {
            return is(T, value - amount);
        }

        pub fn times(amount: T) type {
            return is(T, value * amount);
        }

        pub fn dividedBy(amount: T) type {
            return is(T, value / amount);
        }

        pub const backwards = struct {
            pub fn thirteen() bool {
                return value == 31;
            }
        };
    };
}

test "13 is thirteen" {
    const assert = std.debug.assert;

    assert(is(u32, 13).thirteen());
    assert(is(u32, 42).not.thirteen());
    assert(is(u32, 26).divisible.by.thirteen());
    assert(is(u32, 169).square.of.thirteen());
    assert(is(u32, 14).greater.than.thirteen());
    assert(is(u32, 12).less.than.thirteen());
    assert(is(u32, 21).within(9).of.thirteen());
    assert(is(u32, 12).plus(1).thirteen());
    assert(is(u32, 14).minus(1).thirteen());
    assert(is(f32, 6.5).times(2).thirteen());
    assert(is(u32, 26).dividedBy(2).thirteen());
}
