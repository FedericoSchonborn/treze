# Treze

An implementation of [`is-thirteen`](https://github.com/jezen/is-thirteen) in Zig.

## Usage

```zig
is(u32, 13).thirteen();
is(u32, 42).not.thirteen();
is(u32, 26).divisible.by.thirteen();
is(u32, 169).square.of.thirteen();
is(u32, 14).greater.than.thirteen();
is(u32, 12).less.than.thirteen();
is(u32, 21).within(9).of.thirteen();
is(u32, 12).plus(1).thirteen();
is(u32, 14).minus(1).thirteen();
is(f32, 6.5).times(2).thirteen();
is(u32, 26).dividedBy(2).thirteen();
```
